//
//  CurrencyTextField.swift
//  window-shopper
//
//  Created by Nadila Dithmal on 6/3/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import UIKit
@IBDesignable

class CurrencyTextField: UITextField {
    
    override func draw(_ rect: CGRect) { //draw rect function to draw the custom currency label in the beginning of the text field
        
        let size: CGFloat = 20
        let currencyLbl = UILabel(frame: CGRect(x: 5, y: (frame.size.height / 2) - size / 2, width: size * 2, height: size))
        currencyLbl.backgroundColor = #colorLiteral(red: 0.8974438406, green: 0.8974438406, blue: 0.8974438406, alpha: 0.8)
        currencyLbl.textAlignment = .center
        currencyLbl.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        currencyLbl.layer.cornerRadius = 5.0
        
        currencyLbl.clipsToBounds = true //added because draw rect takes away the normal behavior and is now bleeding off the edges
        
        let formatter = NumberFormatter()   //create a number formatter
        formatter.numberStyle = .currency   //assign properties to the formatter
        formatter.locale = .current
        currencyLbl.text = formatter.currencySymbol     //set currencyLbl text to the formatter's currency symbol property
        addSubview(currencyLbl)
        
    }
    
    override func prepareForInterfaceBuilder() {    //  calls the prepare for interface builder prebuilt function
        customizedView()
    }
    
    override func awakeFromNib() {
        customizedView()                            // calls the custom view code
    }
    func customizedView() {                         // custom view code inside a special funciton
        super.awakeFromNib()
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
        layer.cornerRadius = 6.0
        textAlignment = .center
        
        clipsToBounds = true    //added because draw rect takes away the normal behavior and is now bleeding off the edges
        
        if let p = placeholder { // if let sequence to safely unwrap the optional 'placeholder'
            let place = NSAttributedString(string: p, attributes: [.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)]) //create constant 'place' with the object 'NSAttributedString'
            attributedPlaceholder = place
            textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
}
