//
//  Wage.swift
//  window-shopper
//
//  Created by Nadila Dithmal on 6/6/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import Foundation

class Wage {
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
}
